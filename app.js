const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const configDB = require('./config/database');

const index = require('./routes/index');
const search = require('./routes/search');
const books = require('./routes/books');

const app = express();

const APP_PORT = 3000;

mongoose.connect(configDB.url);

app.listen(APP_PORT, () => {
  console.log(`App is running. Open http://localhost:${APP_PORT} in your browser.`);
});

// view engine setup
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/search', search);
app.use('/books', books);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500)
  .json({message:err.message, error:err});
  // res.render('error.html');
});

module.exports = app;
