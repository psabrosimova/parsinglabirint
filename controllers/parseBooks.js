const Parser = require("../model/Parser");
const Book = require("../model/Book");

const parseBooks = async (queryText) => {
    const parser = new Parser();
    const books = await parser.parseLabirint();

    books.forEach(bookItem => {
        const book = new Book({
            ...bookItem
        });

        book.collection.findOne({ title: bookItem.title }, (err, foundBook) => {
            if (err) {
                console.error('Book find error', err);
            }

            if (foundBook) {
                return book.replaceOne({ title: bookItem.title }, {...bookItem}, (err) => {
                    if (err) {
                        return console.error('Book update error', err);
                    }
                });
            }

            book.save(err => {
                if (err) {
                    return console.error('Book save error', err);
                }
            });
        });
    });

    return books;
};

const getBooks = async () => {
    const books = Book.find({});

    return books;
};

module.exports = {
    parseBooks,
    getBooks
};