const Nightmare = require("nightmare");

class Parser {
    constructor() {
        this.parser = new Nightmare({
            show: false
        });
    }

    async parseLabirint() {
        const url = "https://www.labirint.ru/genres/1852/?order=popularity&way=forward&onpage=100&display=annotation";
        const books = await this.parser
            .goto(url)
            .wait(".products-row")
            .evaluate(() => {
                var booksElements = [...document.querySelectorAll(".product-annotation")];
                var books = booksElements.map(someBook => {
                    const titleEl = someBook.querySelector(".product-title");
                    const authorEl = someBook.querySelector(".product-author");
                    const priceEl = someBook.querySelector(".price-val span");
                    const annotationEl = someBook.querySelector(".annotation");
                    const coverEl = someBook.querySelector(".cover img");

                    return {
                        title: titleEl && titleEl.innerText,
                        author: authorEl && authorEl.innerText,
                        price: priceEl && parseInt(priceEl.innerText),
                        annotation: annotationEl && annotationEl.innerText,
                        cover: coverEl && coverEl.getAttribute('src')
                    }
                });
                return books;
            })
            .end();

        return books;
    }
}

module.exports = Parser;