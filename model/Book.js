const mongoose = require('mongoose');

const bookScheme = mongoose.Schema({
    title: String,
    author: String,
    price: String,
    annotation: String,
    cover: String,
});

module.exports = mongoose.model('Book', bookScheme);