var express = require('express');
var router = express.Router();

const newsAnalysis = require("../controllers/parseBooks");

/* GET users listing. */
router.post('/', async (req, res) => {
    const response = await newsAnalysis.parseBooks();
    res.status(200).json({
        result: response,
    });
});

router.get('/books', async (req, res) => {
    const response = await newsAnalysis.getBooks();

    res.status(200).json({
        result: response,
    });
});

module.exports = router;

