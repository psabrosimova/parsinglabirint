var express = require('express');
var router = express.Router();

const newsAnalysis = require("../controllers/parseBooks");


router.get('/', async (req, res) => {
    const response = await newsAnalysis.getBooks();

    res.status(200).json({
        result: response,
    });
});

module.exports = router;
